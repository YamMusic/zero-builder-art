<?php

  namespace ZeroBuilderArt;

  if ( !class_exists( 'Autoloader' ) ) {

    class Autoloader {

      /**
       * Set Vars to define directory paths
       * @access private
       */
      static private $_rootPath,
                     $_configPath,
                     $_vendorLibsPath,
                     $_libsPath,
                     $_adminPath,
                     $_controllersPath,
                     $_modelsPath,
                     $_viewsPath,
                     $_assetsPath,
                     $_vendorAssetsPath,
                     $_autoload;

      /**
       *------------------------------------------------------
       * Initializer
       *------------------------------------------------------
       *  Defining the directory paths
       *
       * @param Array $options
       * @return Void
       */
      static public function _init( $options = array() ) {
        extract( $options );

        // Defining the core directory paths
        self::$_rootPath = ZBA_ROOT_PATH;
        self::$_libsPath = ZBA_LIBS_PATH;
        self::$_configPath = ( self::$_rootPath . "config" . PS );

        // Check if the autoload json file exists and load it.
        self::_init_autoloader_file();

        // spl_autoload_register(array($this,'load_controller'));
        // spl_autoload_register(array($this,'load_model'));
        // spl_autoload_register(array($this,'load_library'));

        // log_message('debug',"Loader Class Initialized");
      }

      /**
       *------------------------------------------------------
       *  Initializer autoloader file
       *------------------------------------------------------
       *  Check if the autoload json file exists and load it.
       *
       * @return Void
       */
      static public function _init_autoloader_file( $options = array() ) {
        extract( $options );

        $file_name = ( isset( $file_name ) ? $file_name : 'autoload' );
        $format = ( isset( $format ) ? $format : 'json' );

        self::$_autoload = self::load_json( $file_name );
      }

      /**
       *------------------------------------------------------
       *  File Exists ?
       *------------------------------------------------------
       *  Check if a file exists and return true, otherwise
       *   return false
       *
       * @return Bool true|false
       */
      static public function file_exists( $file_name, $ext = 'php', $options = array() ) {
        extract($options);

        $valid_paths = self::get_valid_paths();
        $file_name = ( $file_name . '.' . $ext );
        $file_exists = $valid_file = false;

        foreach( $valid_paths as $path ) {
          if ( file_exists( $path . $file_name ) ) {
            $file_exists = true;
            $valid_file = ( $path . $file_name );
            break;
          }
        }
        if ( isset( $return_path ) ) return $valid_file;
        return $file_exists;
      }

      /**
       *------------------------------------------------------
       *  Get valid paths
       *------------------------------------------------------
       *  Return a list of the valid paths
       *
       * @return Array [..]|false
       */
      static public function get_valid_paths() {
        $valid_paths = array();

        if ( !empty( self::$_rootPath ) ) $valid_paths[] = self::$_rootPath;
        if ( !empty( self::$_configPath ) ) $valid_paths[] = self::$_configPath;
        if ( !empty( self::$_vendorLibsPath ) ) $valid_paths[] = self::$_vendorLibsPath;
        if ( !empty( self::$_libsPath ) ) $valid_paths[] = self::$_libsPath;
        if ( !empty( self::$_adminPath ) ) $valid_paths[] = self::$_adminPath;
        if ( !empty( self::$_controllersPath ) ) $valid_paths[] = self::$_controllersPath;
        if ( !empty( self::$_modelsPath ) ) $valid_paths[] = self::$_modelsPath;
        if ( !empty( self::$_assetsPath ) ) $valid_paths[] = self::$_assetsPath;
        if ( !empty( self::$_viewsPath ) ) $valid_paths[] = self::$_viewsPath;
        if ( !empty( self::$_vendorAssetsPath ) ) $valid_paths[] = self::$_vendorAssetsPath;

        return $valid_paths;
      }

      /**
       *------------------------------------------------------
       *  Get content from a file
       *------------------------------------------------------
       *  Check if a file exists and return the contents,
       *   otherwise return false
       *
       * @return Mixed String|false
       */
      static public function get_content( $file_name, $ext = 'php', $options = array() ) {
        $options['return_path'] = true;
        $content_path = self::file_exists( $file_name, $ext, $options );

        if ( $content_path ) {
          return file_get_contents( $content_path );
        }

        return false;
      }

      /**
       *------------------------------------------------------
       *  Load JSON
       *------------------------------------------------------
       *  Check if a JSON file exists and convert the contents
       *   to PHP Object
       *
       * @return Mixed Array|false
       */
      static public function load_json( $file_name, $ext = 'json', $options = array() ) {
        $json_path = self::file_exists( $file_name, $ext, array( 'return_path' => true ) );
        $content_json = self::get_content( $file_name, $ext );

        if ( $json_path && $content_json ) {
          return json_decode( $content_json );
        }

        return false;
      }

    /**
     *-----------------------------------------------------
     * Load Library
     *-----------------------------------------------------
     *  Method for load library.
     *  This method return class object.
     *
     * @library String
     * @param String
     * @access public
     */
    public function load_library($library, $param = null) {
      if (is_string($library)) {
        return self::$initialize_class($library);
      }

      if (is_array($library)) {
        foreach ($library as $key) {
          return self::$initialize_class($library);
        }
      }
    }

    /**
     *-----------------------------------------------------
     * Initialize Class
     *-----------------------------------------------------
     * Method for initialise class
     * This method return new object.
     * This method can initialize more class using (array)
     *
     * @library String|Array
     * @param String
     * @access public
     */
    public function initialize_class($library) {
      try {

        if (is_array($library)) {

          foreach($library as $class) {
            $arrayObject =  new $class;
          }

          return $this;
        }

        if (is_string($library)) {
          $stringObject = new $library;
        } else {
          throw new Exception('Class name must be string.');
        }

        if (is_null($library)) {
          throw new Exception('You must enter the name of the class.');
        }

      } catch(Exception $exception) {
        print_r($exception);
      }
    }

    /**
     * Autoload Controller class
     *
     * @param  string $class
     * @return object
     */

    public function load_controller($controller) {
        if ($controller) {
            set_include_path(self::$_controllersPath);
            spl_autoload_exts('.php');
            spl_autoload($class);
        }
    }


      /**
     * Autoload Model class
     *
     * @param  string $class
     * @return object
     */

    public function load_models($model) {
        if ($model) {
            set_include_path(self::$modelsPath);
            spl_autoload_exts('.php');
            spl_autoload($class);
        }
    }

      /**
     * Autoload Library class
     *
     * @param  string $class
     * @return object
     */

    /*public function load_library($library)
    {
        if ($library) {
            set_include_path(self::$_libsPath);
            spl_autoload_exts('.php');
            spl_autoload($class);
        }
    }*/
    }
  }
