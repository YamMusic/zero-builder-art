<?php

  namespace ZeroBuilderArt;

  /**
   *------------------------------------------------
   * Translate
   *------------------------------------------------
   * Module Translator to Zero Builder Core,
   * Translate Module is used as alternative to I18n,
   * work with different formats as:
   *   yaml, json, PHP Array or PHP stdClass
   *
   * @package     ZeroBuilderArt
   * @version     0.1.14
   * @author      Zero Builder Team (Hackett YM)
   * @copyright   2016 - Zero Builder Team (Hackett YM)
   * @license     GPL3 (see gpl-v3.text and license.txt)
   */

  class Translate {

    /**
     * @var stdClass Config object
     */
    protected static $_config;

    /**
     * Init method
     * @param string $locale Target language
     * @param string $path Dictionary path
     */
    static public function _init( $locale = null, $path = null ) {
        self::$conf = new \stdClass;
        self::setLocale($locale);
        self::setDictionary($path);
    }
    /**
    * Sets the current locale
    * @param string $locale
    */
    private static function setLocale($locale=null)
    {
        isset($locale)
            ? self::$conf->locale = $locale
            : self::$conf->locale = null;
    }
    /**
     * Sets the dictionary path
     * @param string $path
     */
    private static function setDictionary($path=null)
    {
        isset($path)
            ? self::$conf->dictionary = include $path
            : self::$conf->dictionary = null;
    }
    /**
    * Returns the translated phrase
    * @param string $phrase The original phrase
    * @return string The translated phrase
    */
    static function __($phrase)
    {
        if(isset(self::$conf->locale) && isset(self::$conf->dictionary))
        {
            return self::$conf->dictionary[$phrase];
        }
        else return $phrase;
    }
}
