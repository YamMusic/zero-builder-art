<?php

  /**
   *-----------------------------------------------------
   * Zero Builder Art
   *-----------------------------------------------------
   * Theme Wordpress for build templates responsive
   * using Bootstrap library.
   *
   * @package ZeroBuilder
   * @version 0.1.7
   * @author Hackett YM <yam.music@hotmail.com>
   * @copyright 2016 - Zero Builder Art
   * @license GPL3 (license.txt)
   */


  try {

    define( 'PS', DIRECTORY_SEPARATOR );
    define( 'ZBA_ROOT_URL', get_template_directory_uri() );
    define( 'ZBA_ROOT_PATH', get_template_directory() . PS );
    define( 'ZBA_CONFIG_PATH', ( ZBA_ROOT_PATH . "config" . PS ) );
    $boot_path = ( ZBA_CONFIG_PATH . 'boot.php' );

    if ( file_exists( $boot_path ) && is_readable( $boot_path ) ) {

      require_once $boot_path;


    } else
      throw new Exception( "File not exists: \"". $boot_path ."\"" );

  } catch ( Exception $exc ) {

    if ( function_exists( 'wp_die' ) ) {
      wp_die( $exc );
    } else {
      die( $exc );
    }

  }
