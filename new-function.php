<?php

  /**
   *-----------------------------------------------------
   *  Zero Builder Art
   *-----------------------------------------------------
   *  Theme Wordpress for build templates responsive
   *  using Bootstrap library.
   *
   * @package     ZeroBuilderArt
   * @version     0.1.14
   * @author      @Yam <yam.music@hotmail.com>
   * @copyright   © 2016 - Zero Builder Art
   * @license     GPL3 (gpl-v3.txt, license.txt)
   */


  try {
    // Defining the directory paths
    define( "PS", DIRECTORY_SEPARATOR );
    define( "ZBA_ROOT_URL", get_template_directory_uri() );
    define( "ZBA_ROOT_PATH", get_template_directory() . PS );
    define( "ZBA_LIBS_PATH", ( ZBA_ROOT_PATH . "libs" . PS ) );
    $autoloader_path = ( ZBA_LIBS_PATH . "autoloader.php" );

    // Check if autoloader file exists and initialize the load.
    if ( file_exists( $autoloader_path ) && is_readable( $autoloader_path ) ) {

      require_once $autoloader_path;
      ZeroBuilderArt\Autoloader::_init();

    } else
      throw new Exception( "File not exists: \"". $autoloader_path ."\"" );

  } catch ( Exception $exc ) {
    // Check if exists the Wordpress function to show errors wp_die;
    if ( function_exists( "wp_die" ) )
      wp_die( $exc->getMessage() );
    else
      die( $exc );

  }
